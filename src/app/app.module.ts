import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http'

import { AppComponent } from './app.component';
import { WeatherComponent } from './weather/weather.component';
import { AppService } from './app.service';
const appRoutes: Routes = [
  {
    path: "",
    component: WeatherComponent
  } ]

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule,

    [RouterModule.forRoot(appRoutes)]
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
