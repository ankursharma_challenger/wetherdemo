import { Injectable } from '@angular/core';
import{ environment } from '../environments/environment'
import { HttpClient,HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AppService {
  private apiUrl: string = environment.apiUrl;

  apikey = 'c113fc77437bb9521e4e7eccd62e9dbe';
  units='metric'
  constructor(private http: HttpClient) { }
  
  getWeather(location,code){
    let param_array = ["q=" + location ,'units='+this.units, "zip="+ code  , "appid=" + this.apikey];
    let request_url = this.apiUrl+ "?" + param_array.join("&");
    return this.http.get<any>(request_url,{});
  }
}
