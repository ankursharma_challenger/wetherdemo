import { Component, OnInit } from '@angular/core';
import {AppService} from '../app.service';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import{ WetherTime } from '../model/wether-time';
import { AllWeather } from '../model/wether-time';
import { Console } from '@angular/core/src/console';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
 
 // weathers: wetherTime = new wetherTime();
 weathers: any =[{lname: 'Mohali', pcode: "160059"},{lname: 'Delhi', pcode: "110003"},{lname: 'Karnal', pcode: "132001"},{lname: 'Mumbai', pcode: "230532"}];

  temperatures: any;
  humidity: any;
  clouds: any;
  timeDt: any;
  wind: any;
  allWeathersData: any = []



  constructor(private appService: AppService) { }

  ngOnInit() {
    this.showWeather();
  }
  showWeather(){
    for(let i=0;i<this.weathers.length;i++){
      let lname = this.weathers[i].lname;
      let pcode = this.weathers[i].pcode;
      this.appService.getWeather(lname,pcode).subscribe(res=>{
          this.parseData(res,lname,pcode);
      });
    }
  }

  parseData(res: any, lname: any, pcode: any){
    const weatherData = new AllWeather();
    if(res.main){
      weatherData.temperature = res.main.temp;
      weatherData.humidity = res.main.humidity;
    }

    if(res.weather && res.weather.length > 0)
      weatherData.weather = res.weather[0].description;

    if(res.wind)
      weatherData.wind = res.wind.speed;

    weatherData.location = lname;
    weatherData.postalCode = pcode;
    this.allWeathersData.push(weatherData);
  }
}
